package mx.unam.ciencias;

import java.util.Scanner;
import mx.unam.ciencias.Pokemon;

/**
 * Clase Principal: En esta clase se implementara el programa
 */

public class PruebaPokemon{

  public static Pokemon[] pokedex = new Pokemon[5];
  public static Scanner sc = new Scanner(System.in);
  public static Pokemon pokemon = new Pokemon(0,null,null,0,0,0,0,0,false);

  public static void main(String[] args){

    System.out.println("****************************************************************\n"+
                       "*                                                              *\n"+
                       "*                     INICIANDO LA POKEDEX                     *\n"+
                       "*                                                              *\n"+
                       "****************************************************************\n");




    System.out.println("\n\nMenu Principal Pokedex\n\n"+
                       "Presione 1 para ingresar Pokemones\n"+
                       "Presiona 0 para salir");
    int menu = sc.nextInt();
    switch (menu) {
      case 1 :
       rellenayimprime();
       break;
      case 0 :
       System.exit(0);
    }
  }

  public static void rellenayimprime(){
    System.out.println("\n\nA continuacion ingresara 5 Pokemones, despues los mostrara el programa");

    for (int i=0;i<pokedex.length;i++) {
      System.out.printf("Ingrese el ID del Pokemon: %d\n",i+1);
      pokemon.setId(sc.nextInt());
      System.out.printf("Ingrese el nombre del Pokemon: %d\n",i+1);
      pokemon.setNombre(sc.next());
      System.out.printf("Ingrese el tipo del Pokemon: %d\n",i+1);
      pokemon.setTipo(sc.next());
      System.out.printf("Ingrese la vida del Pokemon: %d\n",i+1);
      pokemon.setVida(sc.nextInt());
      System.out.printf("Ingrese la cantidad de ataque del Pokemon: %d\n",i+1);
      pokemon.setAtaque(sc.nextInt());
      System.out.printf("Ingrese la cantidad de defensa del Pokemon: %d\n",i+1);
      pokemon.setDefensa(sc.nextInt());
      System.out.printf("Ingrese la velocidad del Pokemon: %d\n",i+1);
      pokemon.setVelocidad(sc.nextInt());
      System.out.printf("Ingrese la generacion del Pokemon: %d\n",i+1);
      pokemon.setGeneracion(sc.nextInt());
      System.out.printf("Es legendario el Pokemon: %d\n",i+1);
      pokemon.setLengendario(sc.nextBoolean());
      pokedex[i] = pokemon;
      System.out.println("\n**********************************************\n"+
                         "*              POKEMON AGREGADO              *\n"+
                         "**********************************************\n\n");
    }

    for (Pokemon i:pokedex)
      System.out.println(i.toString());
  }
}
