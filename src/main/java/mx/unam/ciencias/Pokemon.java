package mx.unam.ciencias;

/**
 * Clase Pokemon: En esta clase se declarara la estructura del programa.
 */
public class Pokemon {


    /* Identificador del Pokemon*/
    private int id;
    /* Nombre del Pokemon*/
    private String nombre;
    /* Tipo de Pokemon*/
    private String tipo;
    /* Cantidad de vida del Pokemon*/
    private int vida;
    /* Cantidad de ataque del Pokemon*/
    private int ataque;
    /* Cantidad de defensa del Pokemon*/
    private int defensa;
    /* Velocidad del Pokemon*/
    private int velocidad;
    /* Tipo de generacion del Pokemon*/
    private int generacion;
    /* Es legendario el Pokemon*/
    private boolean legendario;

    public Pokemon(int id,String nombre,String tipo,int vida,int ataque,
                   int defensa,int velocidad,int generacion,boolean legendario){
                   this.id = id;
                   this.nombre = nombre;
                   this.tipo = tipo;
                   this.vida = vida;
                   this.ataque = ataque;
                   this.defensa = defensa;
                   this.velocidad = velocidad;
                   this.generacion = generacion;
                   this.legendario = legendario;
    }

    /**
     * Metodo para acceder a la propiedad id
     */
    public int getId(){
      return this.id;
    }

    /**
     * Metodo para definir la propiedad id
     */
    public void setId(int id){
      this.id = id;
    }

    /**
     * Metodo para acceder a la propiedad Nombre
     */
    public String getNombre(){
      return this.nombre;
    }

    /**
     * Metodo para definir la propiedad Nombre
     */
    public void setNombre(String nombre){
      this.nombre = nombre;
    }

    /**
     * Metodo para acceder a la propiedad tipo
     */
    public int getTipo(){
      return this.id;
    }

    /**
     * Metodo para definir la propiedad tipo
     */
    public void setTipo(String tipo){
      this.tipo = tipo;
    }

    /**
     * Metodo para acceder a la propiedad vida
     */
    public int getVida(){
      return this.vida;
    }

    /**
     * Metodo para definir la propiedad vida
     */
    public void setVida(int vida){
      this.vida = vida;
    }

    /**
     * Metodo para acceder a la propiedad ataque
     */
    public int getAtaque(){
      return this.ataque;
    }

    /**
     * Metodo para definir la propiedad ataque
     */
    public void setAtaque(int ataque){
      this.ataque = ataque;
    }

    /**
     * Metodo para acceder a la propiedad defensa
     */
    public int getDefensa(){
      return this.defensa;
    }

    /**
     * Metodo para definir la propiedad id
     */
    public void setDefensa(int defensa){
      this.defensa = defensa;
    }

    /**
     * Metodo para acceder a la propiedad velocidad
     */
    public int getVelocidad(){
      return this.velocidad;
    }

    /**
     * Metodo para definir la propiedad velocidad
     */
    public void setVelocidad(int velocidad){
      this.velocidad = velocidad;
    }

    /**
     * Metodo para acceder a la propiedad generacion
     */
    public int getGeneracion(){
      return this.generacion;
    }

    /**
     * Metodo para definir la propiedad generacion
     */
    public void setGeneracion(int generacion){
      this.generacion = generacion;
    }

    /**
     * Metodo para acceder a la propiedad legendario
     */
    public boolean getLegendario(){
      return this.legendario;
    }

    /**
     * Metodo para definir la propiedad legendario
     */
    public void setLengendario(boolean legendario){
      this.legendario = legendario;
    }

    /**
     * Metodo toString para pokemon
     */
     @Override public String toString(){
       String cadena = String.format("I.D        : %d\n"+
                                     "Nombre     : %s\n"+
                                     "Tipo       : %s\n"+
                                     "vida       : %d\n"+
                                     "Ataque     : %d\n"+
                                     "Defensa    : %d\n"+
                                     "Velocidad  : %d\n"+
                                     "Generacion : %d\n"+
                                     "Legendario : %b\n",
                                      id, nombre, tipo, vida, ataque, defensa,
                                      velocidad, generacion, legendario);
      return cadena;
     }

    /**
     * Metodo equals para comparar pokemones
     */
     @Override public boolean equals(Object obj){
       if (!(obj instanceof Pokemon))
            return false;
        Pokemon pokemon = (Pokemon)obj;
        if (pokemon == null)
           return false;
        if (this.id != (pokemon.id))
           return false;
        if (!this.nombre.equals(pokemon.nombre))
           return false;
        if (!this.tipo.equals(pokemon.tipo))
           return false;
        if (this.vida != (pokemon.vida))
           return false;
        if (this.ataque != (pokemon.ataque))
           return false;
        if (this.defensa != (pokemon.defensa))
           return false;
        if (this.velocidad != (pokemon.velocidad))
           return false;
        if (this.generacion != (pokemon.generacion))
           return false;
        if (this.legendario != (pokemon.legendario))
           return false;
        return true;
     }
}
